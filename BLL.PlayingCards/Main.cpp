// C++ Assignment 3 Playing Cards
// Bret L Lewis

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO = 2,
	THREE,
	FOUR, 
	FIVE,
	SIX,
	SEVEN, 
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	HEARTS,
	DIAMONDS,
	SPADES,
	CLUBS
};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{

	_getch();
	return 0;
}